import firebase from 'firebase/app';
import 'firebase/firestore';
const firebaseConfig = firebase.initializeApp({
    apiKey: "AIzaSyDOtVDOO10k4bTVEwCseYpR4Edrno5spns",
    authDomain: "todolist-myteam.firebaseapp.com",
    databaseURL: "https://todolist-myteam.firebaseio.com",
    projectId: "todolist-myteam",
    storageBucket: "todolist-myteam.appspot.com",
    messagingSenderId: "718148110451",
    appId: "1:718148110451:web:472b90000e969dc07ac206",
    measurementId: "G-XC9X0203Z2"
})

export { firebaseConfig as firebase }