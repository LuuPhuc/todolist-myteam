import React from 'react';
import { FaPizzaSlice } from 'react-icons/fa';

export const Header = () => {
    return (
        <header className='header'>
            <nav>
                <img src='images/logo.png' />
                <div className='settings' atl='Todolist'>
                    <ul>
                        <li></li>
                        <li>
                            <FaPizzaSlice />
                        </li>
                        <li></li>
                        <li></li>
                    </ul>
                </div>
            </nav>
        </header>
    )
}