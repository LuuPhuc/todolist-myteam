import { useState, useEffect } from 'react';
import { firebase } from '../firebase';
import { collatedTaskExist } from '../helpers';
export const userTasks = selectedProject => {
    const [tasks, setTasks] = useState([]);

    useEffect(() => {
        let unsubscride = firebase
            .firestore()
            .collection('tasks')
            .where('userId', '==', '');

        unsubscride = selectedProject && !collatedTaskExist(selectedProject)
            ? (unsubscride = unsubscride.where('projectId', '==', selectedProject))
            : selectedProject === 'TODAY'
                ? (unsubscride = unsubscride.where(
                    'date',
                    '==',
                    moment().format('DD/MM/YYY')
                ))
                : selectedProject === 'INBOX' || selectedProject === 0
                    ? (unsubscride = unsubscride.where('date', '==', ''))
                    : unsubscride;
    }, [selectedProject]);
}